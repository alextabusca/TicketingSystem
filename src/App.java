import dataAccess.JDBConnectionWrapper;
import presentation.LoginGUI;

public class App {

    public static void main(String[] args) {
        new JDBConnectionWrapper();

        new LoginGUI();
    }
}