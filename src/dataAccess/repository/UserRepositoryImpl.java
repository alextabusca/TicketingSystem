package dataAccess.repository;

import dataAccess.dbModel.UserDAO;
import dataAccess.JDBConnectionWrapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {

    private UserDAO getUserFromResultSet(ResultSet resultSet) throws SQLException {
        UserDAO userDAO = new UserDAO();

        userDAO.setUserID(resultSet.getInt("user_id"));
        userDAO.setUsername(resultSet.getString("username"));
        userDAO.setPassword(resultSet.getString("password"));
        userDAO.setAdmin(resultSet.getInt("is_admin"));

        return userDAO;
    }

    @Override
    public List<UserDAO> getAllUsers() {
        Connection connection = JDBConnectionWrapper.getConnection();
        List<UserDAO> userList = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM `user_table`";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next())
                userList.add(getUserFromResultSet(resultSet));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList;
    }

    @Override
    public UserDAO getUserByID(int id) {
        Connection connection = JDBConnectionWrapper.getConnection();
        UserDAO user = null;

        try {
            String query = "SELECT * FROM `user_table` WHERE user_id = ?";
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, id);
            System.out.println(statement);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
                user = getUserFromResultSet(resultSet);
            else
                System.out.println("No results found!");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    @Override
    public UserDAO loginUser(String username, String password) {
        Connection connection = JDBConnectionWrapper.getConnection();
        UserDAO user = null;
        try {
            String query = "SELECT * FROM `user_table` WHERE username= ? AND password= ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, username);
            statement.setString(2, password);
            System.out.println(statement);
            ResultSet rs = statement.executeQuery();
            if (rs.next())
                user = getUserFromResultSet(rs);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public List<UserDAO> getByUserType(int isAdmin) {
        Connection connection = JDBConnectionWrapper.getConnection();
        List<UserDAO> UserDAOList = new ArrayList<>();

        try {
            String query = "SELECT * FROM `user_table` WHERE is_admin= ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, isAdmin);
            System.out.println(statement);
            ResultSet rs = statement.executeQuery();

            while (rs.next())
                UserDAOList.add(getUserFromResultSet(rs));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return UserDAOList;
    }

    @Override
    public int createUser(UserDAO user) {
        Connection connection = JDBConnectionWrapper.getConnection();

        try {
            String query = "INSERT INTO `user_table` (username, password, is_admin)" + "VALUES(?,?,?)";
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setInt(3, user.getIsAdmin());

            System.out.println(statement);
            int id = statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next())
                return resultSet.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public boolean updateUser(int userID, UserDAO user) {
        Connection connection = JDBConnectionWrapper.getConnection();
        try {
            String query = "UPDATE `user_table` SET username=?, password=?, is_admin=? " +
                    "WHERE user_id=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setInt(3, user.getIsAdmin());
            statement.setInt(4, userID);

            System.out.println(statement);
            statement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteUserByID(int userID) {
        Connection connection = JDBConnectionWrapper.getConnection();

        try {
            String query = "DELETE FROM `user_table` WHERE user_id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, userID);
            System.out.println(statement);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
