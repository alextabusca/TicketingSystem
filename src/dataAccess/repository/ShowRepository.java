package dataAccess.repository;

import dataAccess.dbModel.ShowDAO;

import java.util.List;

public interface ShowRepository {

    List<ShowDAO> getAllShows();

    ShowDAO getShowByID(int showID);

    List<ShowDAO> getShowsByTitle(String title);

    List<ShowDAO> getShowsByGenre(String genre);

    int createShow(ShowDAO showDAO);

    boolean updateShow(int showID, ShowDAO showDAO);

    boolean deleteShowByID(int showID);

}
