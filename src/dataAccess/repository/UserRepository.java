package dataAccess.repository;

import dataAccess.dbModel.UserDAO;

import java.util.List;

public interface UserRepository {

    List<UserDAO> getAllUsers();

    UserDAO getUserByID(int userID);

    UserDAO loginUser(String username, String password);

    List<UserDAO> getByUserType(int isAdmin);

    int createUser(UserDAO userDAO);

    boolean updateUser(int userID, UserDAO userDAO);

    boolean deleteUserByID(int userID);

}
