package dataAccess.repository;

import dataAccess.JDBConnectionWrapper;
import dataAccess.dbModel.TicketDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TicketRepositoryImpl implements TicketRepository {

    private TicketDAO getTicketFromResultSet(ResultSet resultSet) throws SQLException {
        TicketDAO ticketDAO = new TicketDAO();

        ticketDAO.setTicketID(resultSet.getInt("ticket_id"));
        ticketDAO.setSeatRow(resultSet.getInt("seat_row"));
        ticketDAO.setSeatNumber(resultSet.getInt("seat_number"));
        ticketDAO.setTicketPrice(resultSet.getInt("ticket_price"));
        ticketDAO.setShowId(resultSet.getInt("show_id"));

        return ticketDAO;
    }

    @Override
    public List<TicketDAO> getAllTickets() {
        Connection connection = JDBConnectionWrapper.getConnection();
        List<TicketDAO> ticketDAOList = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM `ticket_table`";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next())
                ticketDAOList.add(getTicketFromResultSet(resultSet));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ticketDAOList;
    }

    @Override
    public TicketDAO getTicketByID(int ticketID) {
        Connection connection = JDBConnectionWrapper.getConnection();
        TicketDAO ticketDAO = null;

        try {
            String query = "SELECT * FROM `ticket_table` WHERE ticket_id = ?";
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, ticketID);
            System.out.println(statement);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
                ticketDAO = getTicketFromResultSet(resultSet);
            else
                System.out.println("No results found!");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ticketDAO;
    }

    @Override
    public List<TicketDAO> getTicketsByShowID(int showID) {
        Connection connection = JDBConnectionWrapper.getConnection();
        List<TicketDAO> ticketDAOList = new ArrayList<>();

        try {
            String query = "SELECT * FROM `ticket_table` WHERE show_id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, showID);
            System.out.println(statement);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next())
                ticketDAOList.add(getTicketFromResultSet(resultSet));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ticketDAOList;
    }

    @Override
    public TicketDAO getTicketByShowAndSeatRowAndSeatNumber(int showID, int seatRow, int seatNumber) {
        Connection connection = JDBConnectionWrapper.getConnection();
        TicketDAO ticketDAO = null;

        try {
            String query = "SELECT * FROM `ticket_table` WHERE show_id = ? AND seat_row = ? AND seat_number = ? ";
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, showID);
            statement.setInt(2, seatRow);
            statement.setInt(3, seatNumber);
            System.out.println(statement);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next())
                ticketDAO = getTicketFromResultSet(resultSet);
            else
                System.out.println("No results found!");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ticketDAO;
    }

    @Override
    public int createTicket(TicketDAO ticketDAO) {
        Connection connection = JDBConnectionWrapper.getConnection();

        try {
            String query = "INSERT INTO `ticket_table` (seat_row, seat_number, ticket_price, show_id)" + "VALUES(?,?,?,?)";
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setInt(1, ticketDAO.getSeatRow());
            statement.setInt(2, ticketDAO.getSeatNumber());
            statement.setInt(3, ticketDAO.getTicketPrice());
            statement.setInt(4, ticketDAO.getShowId());

            System.out.println(statement);
            int id = statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next())
                return resultSet.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public boolean updateTicket(int ticketID, TicketDAO ticketDAO) {
        Connection connection = JDBConnectionWrapper.getConnection();

        try {
            String query = "UPDATE `ticket_table` SET seat_row=?, seat_number=?, ticket_price = ?, show_id = ? " +
                    "WHERE ticket_id=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, ticketDAO.getSeatRow());
            statement.setInt(2, ticketDAO.getSeatNumber());
            statement.setInt(3, ticketDAO.getTicketPrice());
            statement.setInt(4, ticketDAO.getShowId());
            statement.setInt(5, ticketID);
            System.out.println(statement);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteTicketByID(int ticketID) {
        Connection connection = JDBConnectionWrapper.getConnection();

        try {
            String query = "DELETE FROM `ticket_table` WHERE ticket_id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, ticketID);
            System.out.println(statement);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
