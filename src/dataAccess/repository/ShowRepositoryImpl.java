package dataAccess.repository;

import dataAccess.JDBConnectionWrapper;
import dataAccess.dbModel.ShowDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ShowRepositoryImpl implements ShowRepository {

    private ShowDAO getShowFromResultSet(ResultSet resultSet) throws SQLException {
        ShowDAO showDAO = new ShowDAO();

        showDAO.setShowID(resultSet.getInt("show_id"));
        showDAO.setTitle(resultSet.getString("title"));
        showDAO.setGenre(resultSet.getString("genre"));
        showDAO.setDistribution(resultSet.getString("distribution"));
        showDAO.setShowDate(resultSet.getDate("show_date"));
        showDAO.setRemainingTickets(resultSet.getInt("remaining_tickets"));
        showDAO.setAvailableTickets(resultSet.getInt("available_tickets"));

        return showDAO;
    }

    @Override
    public List<ShowDAO> getAllShows() {
        Connection connection = JDBConnectionWrapper.getConnection();
        List<ShowDAO> showDAOList = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM `show_table`";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next())
                showDAOList.add(getShowFromResultSet(resultSet));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return showDAOList;
    }

    @Override
    public ShowDAO getShowByID(int showID) {
        Connection connection = JDBConnectionWrapper.getConnection();
        ShowDAO showDAO = null;

        try {
            String query = "SELECT * FROM `show_table` WHERE show_id=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, showID);
            System.out.println(statement);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
                showDAO = getShowFromResultSet(resultSet);
            else
                System.out.println("No results found!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return showDAO;
    }

    @Override
    public List<ShowDAO> getShowsByTitle(String title) {
        Connection connection = JDBConnectionWrapper.getConnection();
        List<ShowDAO> showDAOList = new ArrayList<>();

        try {
            String query = "SELECT * FROM `show_table` WHERE title = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, title);
            System.out.println(statement);

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next())
                showDAOList.add(getShowFromResultSet(resultSet));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return showDAOList;
    }


    @Override
    public List<ShowDAO> getShowsByGenre(String genre) {
        Connection connection = JDBConnectionWrapper.getConnection();
        List<ShowDAO> showDAOList = new ArrayList<>();

        try {
            String query = "SELECT * FROM `show_table` WHERE genre = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, genre);
            System.out.println(statement);

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next())
                showDAOList.add(getShowFromResultSet(resultSet));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return showDAOList;
    }

    @Override
    public int createShow(ShowDAO showDAO) {
        Connection connection = JDBConnectionWrapper.getConnection();

        try {
            String query = "INSERT INTO `show_table` (title, genre, distribution, show_date, available_tickets, remaining_tickets) " +
                    "VALUES (?, ?, ?, ? ,?, ?)";
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, showDAO.getTitle());
            statement.setString(2, showDAO.getGenre());
            statement.setString(3, showDAO.getDistribution());
            statement.setDate(4, showDAO.getShowDate());
            statement.setInt(5, showDAO.getAvailableTickets());
            statement.setInt(6, showDAO.getRemainingTickets());

            System.out.println(statement);

            int id = statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next())
                return resultSet.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public boolean updateShow(int showID, ShowDAO showDAO) {
        Connection connection = JDBConnectionWrapper.getConnection();

        try {
            String query = "UPDATE `show_table` SET title=?, genre=?, distribution=?, show_date=?, available_tickets=?, remaining_tickets=? " +
                    "WHERE show_id=?";
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setString(1, showDAO.getTitle());
            statement.setString(2, showDAO.getGenre());
            statement.setString(3, showDAO.getDistribution());
            statement.setDate(4, showDAO.getShowDate());
            statement.setInt(5, showDAO.getAvailableTickets());
            statement.setInt(6, showDAO.getRemainingTickets());
            statement.setInt(7, showID);

            System.out.println(statement);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public boolean deleteShowByID(int showID) {
        Connection connection = JDBConnectionWrapper.getConnection();

        try {
            String query = "DELETE FROM `show_table` WHERE show_id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, showID);
            System.out.println(statement);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
