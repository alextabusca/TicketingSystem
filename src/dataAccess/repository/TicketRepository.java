package dataAccess.repository;

import dataAccess.dbModel.TicketDAO;

import java.util.List;

public interface TicketRepository {

    List<TicketDAO> getAllTickets();

    TicketDAO getTicketByID(int ticketID);

    List<TicketDAO> getTicketsByShowID(int showID);

    TicketDAO getTicketByShowAndSeatRowAndSeatNumber(int showID, int seatRow, int seatNumber);

    int createTicket(TicketDAO ticketDAO);

    boolean updateTicket(int ticketID, TicketDAO ticketDAO);

    boolean deleteTicketByID(int ticketID);

}
