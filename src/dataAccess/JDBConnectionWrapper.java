package dataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBConnectionWrapper {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/ticketingSystem";

    private static final String USER = "root";
    private static final String PASS = "";
    //private static final int TIMEOUT = 5;

    private static Connection connection;

    public JDBConnectionWrapper() {
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            createTables();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    private void createTables() throws SQLException {
        Statement statement = connection.createStatement();

        String createUserTable = "CREATE TABLE IF NOT EXISTS user_table (" +
                "   user_id INT(11) NOT NULL AUTO_INCREMENT, " +
                "   username VARCHAR(25) NOT NULL," +
                "   password VARCHAR(40) NOT NULL," +
                "   is_admin INT(2) NOT NULL," +
                "   PRIMARY KEY(user_id)," +
                "   UNIQUE KEY id_UNIQUE (user_id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";

        String createShowTable = "CREATE TABLE IF NOT EXISTS show_table (" +
                "   show_id INT(11) NOT NULL AUTO_INCREMENT," +
                "   title VARCHAR(30) NOT NULL," +
                "   genre VARCHAR(30) NOT NULL ," +
                "   distribution VARCHAR(35) NOT NULL," +
                "   show_date DATE," +
                "   remaining_tickets INT(10) NOT NULL," +
                "   available_Tickets INT(10) NOT NULL," +
                "   PRIMARY KEY (show_id)," +
                "   UNIQUE KEY id_UNIQUE(show_id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHAR SET=utf8;";

        String createTicketTable = "CREATE TABLE IF NOT EXISTS ticket_table (" +
                "   ticket_id INT(11) NOT NULL AUTO_INCREMENT, " +
                "   seat_row INT(10) NOT NULL," +
                "   seat_number INT(10) NOT NULL," +
                "   ticket_price INT(10) NOT NULL, " +
                "   show_id INT(11) NOT NULL," +
                "   PRIMARY KEY(ticket_id)," +
                "   FOREIGN KEY(show_id) REFERENCES show_table (show_id)," +
                "   UNIQUE KEY id_UNIQUE (ticket_id)" +
                ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";

        statement.execute(createUserTable);
        statement.execute(createShowTable);
        statement.execute(createTicketTable);
    }

    /*public boolean testConnection() throws SQLException {
        return connection.isValid(TIMEOUT);
    }*/

    public static Connection getConnection() {
        return connection;
    }
}
