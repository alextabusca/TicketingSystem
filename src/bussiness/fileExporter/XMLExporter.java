package bussiness.fileExporter;

import bussiness.model.TicketDTO;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

public class XMLExporter implements Export {

    private static final String xmlFilePath = "C:\\Users\\Alex\\Desktop\\Tickets.xml";

    @Override
    public void export(List<TicketDTO> TicketDTOList) {

        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();


            Element root = document.createElement("Tickets");
            document.appendChild(root);

            for (TicketDTO TicketDTO : TicketDTOList) {
                Element ticket = document.createElement("Ticket");
                root.appendChild(ticket);
                Attr attr = document.createAttribute("TicketId");
                attr.setValue(String.valueOf(TicketDTO.getTicketID()));
                ticket.setAttributeNode(attr);

                Element showID = document.createElement("ShowId");
                showID.appendChild(document.createTextNode(String.valueOf(TicketDTO.getShowId())));
                ticket.appendChild(showID);

                Element seatRow = document.createElement("SeatRow");
                seatRow.appendChild(document.createTextNode(String.valueOf(TicketDTO.getSeatRow())));
                ticket.appendChild(seatRow);

                Element seatNumber = document.createElement("SeatNumber");
                seatNumber.appendChild(document.createTextNode(String.valueOf(TicketDTO.getSeatNumber())));
                ticket.appendChild(seatNumber);

                Element ticketPrice = document.createElement("TicketPrice");
                ticketPrice.appendChild(document.createTextNode(String.valueOf(TicketDTO.getTicketPrice())));
                ticket.appendChild(ticketPrice);

                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource domSource = new DOMSource(document);
                StreamResult streamResult = new StreamResult(new File(xmlFilePath));

                transformer.transform(domSource, streamResult);
            }
            System.out.println("Done creating XML File");

        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
    }
}
