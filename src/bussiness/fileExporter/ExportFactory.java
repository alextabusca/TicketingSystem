package bussiness.fileExporter;

public class ExportFactory {

    public static Export getExportType(String format) {
        Export export = null;

        if (format.equalsIgnoreCase("XML")) {
            export = new XMLExporter();
        } else if (format.equalsIgnoreCase("CSV")) {
            export = new CSVExporter();
        }

        return export;
    }
}
