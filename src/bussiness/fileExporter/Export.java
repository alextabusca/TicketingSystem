package bussiness.fileExporter;


import bussiness.model.TicketDTO;

import java.util.List;

public interface Export {

    void export(List<TicketDTO> ticketDAOList);
}
