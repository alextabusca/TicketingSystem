package bussiness.fileExporter;


import bussiness.model.TicketDTO;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CSVExporter implements Export {

    private static final String COMMA_DELIMITER = ",";
    private static final String LINE_SEPARATOR = "\n";

    private static final String HEADER = "TicketId,ShowId,SeatRow,SeatNumber,Price";

    @Override
    public void export(List<TicketDTO> ticketDTOList) {

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("C:\\Users\\Alex\\Desktop\\Tickets.csv");

            //Adding the header
            fileWriter.append(HEADER);
            //New Line after the header
            fileWriter.append(LINE_SEPARATOR);

            //Iterate the empList
            for (TicketDTO ticketDTO : ticketDTOList) {
                fileWriter.append(String.valueOf(ticketDTO.getTicketID()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(ticketDTO.getShowId()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(ticketDTO.getSeatRow()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(ticketDTO.getSeatNumber()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(ticketDTO.getTicketPrice()));
                fileWriter.append(LINE_SEPARATOR);
            }
            System.out.println("Write to CSV file Succeeded!!!");
        } catch (Exception ee) {
            ee.printStackTrace();
        } finally {
            try {
                assert fileWriter != null;
                fileWriter.close();
            } catch (IOException ie) {
                System.out.println("Error occurred while closing the fileWriter");
                ie.printStackTrace();
            }
        }

    }
}
