package bussiness.model;

import java.sql.Date;

public class ShowDTO {

    private int showID;
    private String title;
    private String genre;
    private String distribution;
    private Date showDate;
    private int remainingTickets;
    private int availableTickets;

    public ShowDTO(int showID, String title, String genre, String distribution, Date showDate, int availableTickets, int remainingTickets) {
        this.showID = showID;
        this.title = title;
        this.genre = genre;
        this.distribution = distribution;
        this.showDate = showDate;
        this.remainingTickets = remainingTickets;
        this.availableTickets = availableTickets;
    }

    public ShowDTO(String title, String genre, String distribution, Date showDate, int availableTickets, int remainingTickets) {
        this.title = title;
        this.genre = genre;
        this.distribution = distribution;
        this.showDate = showDate;
        this.remainingTickets = remainingTickets;
        this.availableTickets = availableTickets;
    }

    public ShowDTO() {

    }

    public int getShowID() {
        return showID;
    }

    public void setShowID(int showID) {
        this.showID = showID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDistribution() {
        return distribution;
    }

    public void setDistribution(String distribution) {
        this.distribution = distribution;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public int getRemainingTickets() {
        return remainingTickets;
    }

    public void setRemainingTickets(int remainingTickets) {
        this.remainingTickets = remainingTickets;
    }

    public int getAvailableTickets() {
        return availableTickets;
    }

    public void setAvailableTickets(int availableTickets) {
        this.availableTickets = availableTickets;
    }

    public String toString() {
        return "Show with id = " + showID + ", title=" + title + ",distribution=" + distribution;
    }
}