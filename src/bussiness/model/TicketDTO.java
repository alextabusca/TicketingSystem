package bussiness.model;

public class TicketDTO {

    private int ticketID;
    private int seatRow;
    private int seatNumber;
    private int showId;
    private int ticketPrice;

    public TicketDTO(int ticketID, int showId, int seatRow, int seatNumber, int ticketPrice) {
        this.ticketID = ticketID;
        this.showId = showId;
        this.seatRow = seatRow;
        this.seatNumber = seatNumber;
        this.ticketPrice = ticketPrice;
    }

    public TicketDTO(int showId, int seatRow, int seatNumber, int ticketPrice) {
        this.showId = showId;
        this.seatRow = seatRow;
        this.seatNumber = seatNumber;
        this.ticketPrice = ticketPrice;
    }

    public TicketDTO() {

    }

    public int getTicketID() {
        return ticketID;
    }

    public void setTicketID(int ticketID) {
        this.ticketID = ticketID;
    }

    public int getShowId() {
        return showId;
    }

    public void setShowId(int showId) {
        this.showId = showId;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public int getSeatRow() {
        return seatRow;
    }

    public void setSeatRow(int seatRow) {
        this.seatRow = seatRow;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }
}
