package bussiness.service;

import bussiness.model.UserDTO;
import dataAccess.dbModel.UserDAO;
import dataAccess.repository.UserRepository;
import dataAccess.repository.UserRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl() {
        this.userRepository = new UserRepositoryImpl();
    }

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDTO> getAllUsers() {
        try {
            return userDAOListToDTOList(userRepository.getAllUsers());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserDTO getUserByID(int userID) {
        try {
            return userDAOToDTO(userRepository.getUserByID(userID));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserDTO loginUser(String username, String password) {
        try {
            return userDAOToDTO(userRepository.loginUser(username, password));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<UserDTO> getByUserType(int isAdmin) {
        try {
            return userDAOListToDTOList(userRepository.getByUserType(isAdmin));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int createUser(UserDTO userDTO) {
        try {
            return userRepository.createUser(userDTOToDAO(userDTO));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public boolean updateUser(int userID, UserDTO userDTO) {
        try {
            return userRepository.updateUser(userID, userDTOToDAO(userDTO));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteUserByID(int userID) {
        try {
            return userRepository.deleteUserByID(userID);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private UserDAO userDTOToDAO(UserDTO userDTO) {
        UserDAO userDAO = new UserDAO();

        userDAO.setUserID(userDTO.getUserID());
        userDAO.setUsername(userDTO.getUsername());
        userDAO.setPassword(userDTO.getPassword());
        userDAO.setAdmin(userDTO.getIsAdmin());

        return userDAO;
    }

    private UserDTO userDAOToDTO(UserDAO userDAO) {
        UserDTO userDTO = new UserDTO();

        userDTO.setUserID(userDAO.getUserID());
        userDTO.setUsername(userDAO.getUsername());
        userDTO.setPassword(userDAO.getPassword());
        userDTO.setAdmin(userDAO.getIsAdmin());

        return userDTO;
    }

    private List<UserDTO> userDAOListToDTOList(List<UserDAO> userDAOList) {
        List<UserDTO> userDTOList = new ArrayList<>();

        for (UserDAO userDAO : userDAOList)
            userDTOList.add(userDAOToDTO(userDAO));

        return userDTOList;
    }
}
