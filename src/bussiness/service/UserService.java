package bussiness.service;

import bussiness.model.UserDTO;

import java.util.List;

public interface UserService {

    List<UserDTO> getAllUsers();

    UserDTO getUserByID(int userID);

    UserDTO loginUser(String username, String password);

    List<UserDTO> getByUserType(int isAdmin);

    int createUser(UserDTO userDTO);

    boolean updateUser(int userID, UserDTO userDTO);

    boolean deleteUserByID(int userID);

}
