package bussiness.service;

import bussiness.model.TicketDTO;

import java.util.List;

public interface TicketService {

    List<TicketDTO> getAllTickets();

    TicketDTO getTicketByID(int ticketID);

    List<TicketDTO> getTicketsByShowID(int showID);

    TicketDTO getTicketByShowAndSeatRowAndSeatNumber(int showID, int seatRow, int seatNumber);

    int createTicket(TicketDTO TicketDTO);

    boolean updateTicket(int ticketID, TicketDTO TicketDTO);

    boolean deleteTicketByID(int ticketID);
}
