package bussiness.service;

import bussiness.model.ShowDTO;

import java.util.List;

public interface ShowService {

    List<ShowDTO> getAllShows();

    ShowDTO getShowByID(int showID);

    List<ShowDTO> getShowsByTitle(String title);

    List<ShowDTO> getShowsByGenre(String genre);

    int createShow(ShowDTO ShowDTO);

    boolean updateShow(int showID, ShowDTO ShowDTO);

    boolean deleteShowByID(int showID);

}
