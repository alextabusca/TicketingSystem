package bussiness.service;

import bussiness.model.TicketDTO;
import dataAccess.dbModel.TicketDAO;
import dataAccess.repository.TicketRepository;
import dataAccess.repository.TicketRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;

    public TicketServiceImpl() {
        this.ticketRepository = new TicketRepositoryImpl();
    }

    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public List<TicketDTO> getAllTickets() {
        try {
            return ticketDAOListToDTOList(ticketRepository.getAllTickets());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public TicketDTO getTicketByID(int ticketID) {
        try {
            return ticketDAOtoDTO(ticketRepository.getTicketByID(ticketID));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TicketDTO> getTicketsByShowID(int showID) {
        try {
            return ticketDAOListToDTOList(ticketRepository.getTicketsByShowID(showID));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public TicketDTO getTicketByShowAndSeatRowAndSeatNumber(int showID, int seatRow, int seatNumber) {
        try {
            return ticketDAOtoDTO(ticketRepository.getTicketByShowAndSeatRowAndSeatNumber(showID, seatRow, seatNumber));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int createTicket(TicketDTO ticketDTO) {
        try {
            return ticketRepository.createTicket(ticketDTOToDAO(ticketDTO));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public boolean updateTicket(int ticketID, TicketDTO ticketDTO) {
        try {
            return ticketRepository.updateTicket(ticketID, ticketDTOToDAO(ticketDTO));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteTicketByID(int ticketID) {
        try {
            return ticketRepository.deleteTicketByID(ticketID);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private TicketDAO ticketDTOToDAO(TicketDTO ticketDTO) {
        TicketDAO ticketDAO = new TicketDAO();

        ticketDAO.setTicketID(ticketDTO.getTicketID());
        ticketDAO.setSeatNumber(ticketDTO.getSeatNumber());
        ticketDAO.setSeatRow(ticketDTO.getSeatRow());
        ticketDAO.setTicketPrice(ticketDTO.getTicketPrice());
        ticketDAO.setShowId(ticketDTO.getShowId());

        return ticketDAO;
    }

    private TicketDTO ticketDAOtoDTO(TicketDAO ticketDAO) {
        TicketDTO ticketDTO = new TicketDTO();

        ticketDTO.setTicketID(ticketDAO.getTicketID());
        ticketDTO.setSeatNumber(ticketDAO.getSeatNumber());
        ticketDTO.setSeatRow(ticketDAO.getSeatRow());
        ticketDTO.setTicketPrice(ticketDAO.getTicketPrice());
        ticketDTO.setShowId(ticketDAO.getShowId());

        return ticketDTO;
    }

    private List<TicketDTO> ticketDAOListToDTOList(List<TicketDAO> ticketDAOList) {
        List<TicketDTO> ticketDTOList = new ArrayList<>();

        for (TicketDAO ticketDAO : ticketDAOList)
            ticketDTOList.add(ticketDAOtoDTO(ticketDAO));

        return ticketDTOList;
    }
}
