package bussiness.service;

import bussiness.model.ShowDTO;
import dataAccess.dbModel.ShowDAO;
import dataAccess.repository.ShowRepository;
import dataAccess.repository.ShowRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

public class ShowServiceImpl implements ShowService {

    private final ShowRepository showRepository;

    public ShowServiceImpl() {
        this.showRepository = new ShowRepositoryImpl();
    }

    public ShowServiceImpl(ShowRepository showRepository) {
        this.showRepository = showRepository;
    }

    @Override
    public List<ShowDTO> getAllShows() {

        try {
            return showDAOListToDTOList(showRepository.getAllShows());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ShowDTO getShowByID(int showID) {
        try {
            return showDAOToDTO(showRepository.getShowByID(showID));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<ShowDTO> getShowsByTitle(String title) {
        try {
            return showDAOListToDTOList(showRepository.getShowsByTitle(title));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<ShowDTO> getShowsByGenre(String genre) {
        try {
            return showDAOListToDTOList(showRepository.getShowsByGenre(genre));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int createShow(ShowDTO showDTO) {
        try {
            return showRepository.createShow(showDTOToDAO(showDTO));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public boolean updateShow(int showID, ShowDTO showDTO) {
        try {
            return showRepository.updateShow(showID, showDTOToDAO(showDTO));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteShowByID(int showID) {
        try {
            return showRepository.deleteShowByID(showID);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private ShowDAO showDTOToDAO(ShowDTO showDTO) {
        ShowDAO showDAO = new ShowDAO();

        showDAO.setShowID(showDTO.getShowID());
        showDAO.setTitle(showDTO.getTitle());
        showDAO.setGenre(showDTO.getGenre());
        showDAO.setDistribution(showDTO.getDistribution());
        showDAO.setAvailableTickets(showDTO.getAvailableTickets());
        showDAO.setRemainingTickets(showDTO.getRemainingTickets());

        return showDAO;
    }

    private ShowDTO showDAOToDTO(ShowDAO showDAO) {
        ShowDTO showDTO = new ShowDTO();

        showDTO.setShowID(showDAO.getShowID());
        showDTO.setTitle(showDAO.getTitle());
        showDTO.setGenre(showDAO.getGenre());
        showDTO.setDistribution(showDAO.getDistribution());
        showDTO.setAvailableTickets(showDAO.getAvailableTickets());
        showDTO.setRemainingTickets(showDAO.getRemainingTickets());

        return showDTO;
    }

    private List<ShowDTO> showDAOListToDTOList(List<ShowDAO> showDAOList) {
        List<ShowDTO> showDTOList = new ArrayList<>();

        for (ShowDAO showDAO : showDAOList)
            showDTOList.add(showDAOToDTO(showDAO));

        return showDTOList;
    }
}
