package presentation.util.table;

import bussiness.model.TicketDTO;
import bussiness.service.TicketService;
import bussiness.service.TicketServiceImpl;

import javax.swing.*;
import java.util.List;

public class TicketTable extends AbstractTable<TicketDTO> {

    private JTable create(List<TicketDTO> ticketDTOList) {
        TicketTable ticketTable = new TicketTable();

        return ticketTable.createTable(ticketDTOList);
    }

    public JTable createTicketsByShow(int showID) {
        TicketService ticketService = new TicketServiceImpl();

        try {
            List<TicketDTO> ticketDTOList = ticketService.getTicketsByShowID(showID);

            return create(ticketDTOList);
        } catch (Exception e) {
            e.printStackTrace();
            return create(null);
        }
    }
}
