package presentation.util.table;

import bussiness.model.ShowDTO;
import bussiness.service.ShowService;
import bussiness.service.ShowServiceImpl;

import javax.swing.*;
import java.util.List;

public class ShowTable extends AbstractTable<ShowDTO> {

    private JTable create(List<ShowDTO> showDTOList) {
        ShowTable showTable = new ShowTable();

        return showTable.createTable(showDTOList);
    }

    public JTable createTable() {
        ShowService showService = new ShowServiceImpl();

        try {
            List<ShowDTO> showDTOList = showService.getAllShows();

            return create(showDTOList);
        } catch (Exception e) {
            e.printStackTrace();
            return create(null);
        }
    }
}
