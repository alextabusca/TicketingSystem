package presentation.util.table;

import bussiness.model.UserDTO;
import bussiness.service.UserService;
import bussiness.service.UserServiceImpl;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class CashierTable extends AbstractTable<UserDTO> {

    private JTable create(List<UserDTO> userDTOList) {
        CashierTable cashierTable = new CashierTable();

        return cashierTable.createTable(userDTOList);
    }

    public JTable createTable() {
        UserService userService = new UserServiceImpl();

        List<UserDTO> userDTOList = userService.getAllUsers();
        List<UserDTO> cashiersList = new ArrayList<>();

        for (UserDTO userDTO : userDTOList)
            if (userDTO.getIsAdmin() == 0)
                cashiersList.add(userDTO);

        return create(cashiersList);
    }
}
