package presentation;

import presentation.util.Encryption;
import bussiness.model.UserDTO;
import bussiness.service.UserService;
import bussiness.service.UserServiceImpl;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class LoginGUI extends JFrame {

    public LoginGUI() {

        super("Login");
        setName("Login Interface");

        setSize(500, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        UserService userService = new UserServiceImpl();

        JPanel userInfoPanel = new JPanel();
        userInfoPanel.setLayout(new GridLayout(2, 2));
        JLabel usernameLabel = new JLabel("Username: ");
        JLabel passwordLabel = new JLabel("Password: ");
        JTextField usernameText = new JTextField(30);
        JPasswordField passwordText = new JPasswordField(30);

        userInfoPanel.add(usernameLabel);
        userInfoPanel.add(usernameText);
        userInfoPanel.add(passwordLabel);
        userInfoPanel.add(passwordText);

        JPanel loginButtonPanel = new JPanel();
        JButton loginButton = new JButton("Login");
        loginButtonPanel.add(loginButton);

        loginButton.addActionListener(e -> {
            String username;
            String password;
            List<UserDTO> userList;

            try {
                username = usernameText.getText();
                password = String.valueOf(passwordText.getText());
                System.out.println(password);
                String encryptedPassword = Encryption.passwordEncryption(password);
                System.out.println(encryptedPassword);
                userList = userService.getAllUsers();

                for (UserDTO user : userList)
                    if (user.getUsername().equals(username) && user.getPassword().equals(encryptedPassword))
                        if (user.getIsAdmin() == 1)
                            new AdminGUI();
                        else
                            new CashierGUI();
            } catch (Exception err) {
                err.printStackTrace();
                JOptionPane.showMessageDialog(null, "Invalid username or password! Please try again!");
            }
        });

        add(userInfoPanel, BorderLayout.CENTER);
        add(loginButton, BorderLayout.SOUTH);

        setLocationRelativeTo(null);
        setVisible(true);
    }

}
