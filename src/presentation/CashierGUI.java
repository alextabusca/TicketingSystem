package presentation;

import bussiness.model.ShowDTO;
import bussiness.model.TicketDTO;
import bussiness.service.ShowService;
import bussiness.service.ShowServiceImpl;
import bussiness.service.TicketService;
import bussiness.service.TicketServiceImpl;
import presentation.util.table.TicketTable;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

class CashierGUI extends JFrame {

    private TicketTable tickets;

    private JPanel ticketTablePanel;

    private JTable newTicketTable;

    private JLabel remainingTicketsNumber;

    private JComboBox showTitleComboBox;

    CashierGUI() {

        /*
        Initializations
         */
        super("TicketingSystem - Cashier");
        setName("Cashier Interface");

        setSize(900, 400);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setResizable(false);

        JPanel finalPanel = new JPanel();

        TicketService ticketService = new TicketServiceImpl();


        /*
        Ticket reservation
         */
        ShowService showService = new ShowServiceImpl();
        List<ShowDTO> showDTOList = showService.getAllShows();
        List<String> showTitleList = new ArrayList<>();
        for (ShowDTO showDTO : showDTOList)
            showTitleList.add(showDTO.getTitle());
        showTitleComboBox = new JComboBox<>(showTitleList.toArray());

        JLabel showTitleLabel = new JLabel("Show: ");
        JLabel ticketRowLabel = new JLabel("Row: ");
        JLabel ticketNumberLabel = new JLabel("Number: ");
        JLabel ticketPriceLabel = new JLabel("Price: ");
        JLabel remainingTicketsLabel = new JLabel("Remaining: ");
        remainingTicketsNumber = new JLabel("");
        JButton ticketReserveButton = new JButton("Reserve");
        JButton ticketEditButton = new JButton("Edit");
        JButton ticketCancelButton = new JButton("Cancel");

        JTextField ticketRowText = new JTextField(5);
        JTextField ticketNumberText = new JTextField(5);
        JTextField ticketPriceText = new JTextField(5);

        remainingTicketsNumber.setText(String.valueOf(showDTOList.get(showTitleComboBox.getSelectedIndex()).getRemainingTickets()));

        showTitleComboBox.addActionListener(e -> {
            //remainingTicketsNumber.setText(String.valueOf(showDTOList.get(showTitleComboBox.getSelectedIndex()).getRemainingTickets()));

            refreshRemainingTicketsNumber();

            refreshTicketsTable(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID());
        });

        JPanel ticketInfoPanel = new JPanel(new GridLayout(7, 2));
        ticketInfoPanel.add(showTitleLabel);
        ticketInfoPanel.add(showTitleComboBox);
        ticketInfoPanel.add(ticketRowLabel);
        ticketInfoPanel.add(ticketRowText);
        ticketInfoPanel.add(ticketNumberLabel);
        ticketInfoPanel.add(ticketNumberText);
        ticketInfoPanel.add(ticketPriceLabel);
        ticketInfoPanel.add(ticketPriceText);
        ticketInfoPanel.add(remainingTicketsLabel);
        ticketInfoPanel.add(remainingTicketsNumber);
        ticketInfoPanel.add(ticketReserveButton);
        ticketInfoPanel.add(ticketEditButton);
        ticketInfoPanel.add(ticketCancelButton);

        /*
        Ticket table
         */
        ticketTablePanel = new JPanel();
        tickets = new TicketTable();
        JTable ticketTable = tickets.createTicketsByShow(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID());
        ticketTable.setPreferredSize(new Dimension(550, 300));
        ticketTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane ticketScrollPane = new JScrollPane(ticketTable);
        ticketScrollPane.setPreferredSize(new Dimension(550, 300));
        ticketTablePanel.add(ticketScrollPane);

        refreshTicketsTable(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID());

        ticketReserveButton.addActionListener(e -> {
            if (showService.getShowByID(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID()).getRemainingTickets() > 0)
                if (ticketService.getTicketByShowAndSeatRowAndSeatNumber(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID(), Integer.parseInt(ticketRowText.getText()), Integer.parseInt(ticketNumberText.getText())) == null) {
                    ticketService.createTicket(new TicketDTO(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID(), Integer.parseInt(ticketRowText.getText()), Integer.parseInt(ticketNumberText.getText()), Integer.parseInt(ticketPriceText.getText())));

                    ShowDTO showSelectedFromComboBox = showService.getShowByID(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID());
                    showService.updateShow(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID(), new ShowDTO(showSelectedFromComboBox.getTitle(), showSelectedFromComboBox.getGenre(), showSelectedFromComboBox.getDistribution(), showSelectedFromComboBox.getShowDate(), showSelectedFromComboBox.getAvailableTickets(), showSelectedFromComboBox.getRemainingTickets() - 1));

                    refreshRemainingTicketsNumber();
                    refreshTicketsTable(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID());
                } else
                    JOptionPane.showMessageDialog(null, "Ticket on that place exists already!");
            else
                JOptionPane.showMessageDialog(null, "Not enough remaining tickets!");
        });

        ticketEditButton.addActionListener(e -> {
            JFrame editTicketFrame = new JFrame();
            JPanel editTicketPanel = new JPanel(new GridLayout(2, 0));
            JPanel editTicketInfoPanel = new JPanel(new GridLayout(3, 2));
            JPanel editTicketSubmitButtonPanel = new JPanel();

            editTicketFrame.setSize(350, 180);
            editTicketFrame.setName("Edit ticket");
            editTicketFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            editTicketFrame.setResizable(false);

            JLabel editTicketRowLabel = new JLabel("Row: ");
            JLabel editTicketNumberLabel = new JLabel("Number: ");
            JLabel editTicketPriceLabel = new JLabel("Price: ");

            JTextField editTicketRowText = new JTextField(5);
            JTextField editTicketNumberText = new JTextField(5);
            JTextField editTicketPriceText = new JTextField(5);

            editTicketInfoPanel.add(editTicketRowLabel);
            editTicketInfoPanel.add(editTicketRowText);
            editTicketInfoPanel.add(editTicketNumberLabel);
            editTicketInfoPanel.add(editTicketNumberText);
            editTicketInfoPanel.add(editTicketPriceLabel);
            editTicketInfoPanel.add(editTicketPriceText);

            JButton editTicketSubmitButton = new JButton("Submit");
            editTicketSubmitButtonPanel.add(editTicketSubmitButton);

            editTicketPanel.add(editTicketInfoPanel);
            editTicketPanel.add(editTicketSubmitButtonPanel);

            editTicketSubmitButton.addActionListener(e1 -> {
                ticketService.updateTicket((int) newTicketTable.getValueAt(newTicketTable.getSelectedRow(), 0), new TicketDTO(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID(), Integer.parseInt(editTicketRowText.getText()), Integer.parseInt(editTicketNumberText.getText()), Integer.parseInt(editTicketPriceText.getText())));

                refreshTicketsTable(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID());
                editTicketFrame.dispose();
            });

            editTicketFrame.add(editTicketPanel);
            editTicketFrame.setVisible(true);
            editTicketFrame.setLocationRelativeTo(null);
        });

        ticketCancelButton.addActionListener(e -> {
            ShowDTO showSelectedFromComboBox = showService.getShowByID(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID());
            showService.updateShow(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID(), new ShowDTO(showSelectedFromComboBox.getTitle(), showSelectedFromComboBox.getGenre(), showSelectedFromComboBox.getDistribution(), showSelectedFromComboBox.getShowDate(), showSelectedFromComboBox.getAvailableTickets(), showSelectedFromComboBox.getRemainingTickets() + 1));
            ticketService.deleteTicketByID((int) newTicketTable.getValueAt(newTicketTable.getSelectedRow(), 0));

            refreshRemainingTicketsNumber();
            refreshTicketsTable(showDTOList.get(showTitleComboBox.getSelectedIndex()).getShowID());
        });

        /*
           Final settings
         */
        finalPanel.add(ticketInfoPanel);
        finalPanel.add(ticketTablePanel);

        add(finalPanel);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void refreshTicketsTable(int showID) {
        newTicketTable = tickets.createTicketsByShow(showID);
        newTicketTable.setPreferredSize(new Dimension(550, 500));
        newTicketTable.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        JScrollPane newPane = new JScrollPane(newTicketTable);
        newPane.setPreferredSize(new Dimension(550, 300));

        ticketTablePanel.removeAll();
        ticketTablePanel.add(newPane);
        ticketTablePanel.revalidate();
        ticketTablePanel.repaint();
    }

    private void refreshRemainingTicketsNumber() {
        ShowService showService = new ShowServiceImpl();
        List<ShowDTO> showDTOList = showService.getAllShows();
        remainingTicketsNumber.setText(String.valueOf(showDTOList.get(showTitleComboBox.getSelectedIndex()).getRemainingTickets()));
    }
}
