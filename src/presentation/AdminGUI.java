package presentation;

import bussiness.fileExporter.ExportFactory;
import bussiness.model.ShowDTO;
import bussiness.model.TicketDTO;
import bussiness.model.UserDTO;
import bussiness.service.*;
import presentation.util.Encryption;
import presentation.util.table.CashierTable;
import presentation.util.table.ShowTable;

import javax.swing.*;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

class AdminGUI extends JFrame {

    private CashierTable cashier;
    private ShowTable show;

    private JPanel cashierTablePanel, showTablePanel;

    private JPanel finalPanel, cashierPanel, showPanel;

    private JTable newCashierTable, newShowTable;

    private JButton switchToCashiersPanel, switchToShowsPanel, exportToCSV, exportToXML;

    AdminGUI() {

        /*
        Initializations
         */
        super("TicketingSystem - Admin");
        setName("Admin Interface");

        setSize(1000, 400);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setResizable(false);
        setLayout(new GridLayout(1, 3));
        finalPanel = new JPanel();

        /*
        Switches and Export
         */
        switchToCashiersPanel = new JButton("Cashiers");
        switchToShowsPanel = new JButton("Shows");
        exportToCSV = new JButton("Export to CSV");
        exportToXML = new JButton("Export to XML");

        finalPanel.add(switchToCashiersPanel);
        finalPanel.add(switchToShowsPanel);
        finalPanel.add(exportToCSV);
        finalPanel.add(exportToXML);

        switchToCashiersPanel.addActionListener(e -> {
            finalPanel.removeAll();
            finalPanel.add(switchToCashiersPanel);
            finalPanel.add(switchToShowsPanel);
            finalPanel.add(exportToCSV);
            finalPanel.add(exportToXML);
            finalPanel.add(cashierPanel);
            finalPanel.revalidate();
            finalPanel.repaint();
            refreshCashiersTable();
        });

        switchToShowsPanel.addActionListener(e -> {
            finalPanel.removeAll();
            finalPanel.add(switchToCashiersPanel);
            finalPanel.add(switchToShowsPanel);
            finalPanel.add(exportToCSV);
            finalPanel.add(exportToXML);
            finalPanel.add(showPanel);
            finalPanel.revalidate();
            finalPanel.repaint();
            refreshShowsTable();
        });

        exportToXML.addActionListener(e -> {
            List<TicketDTO> ticketDTOList;

            TicketService ticketService = new TicketServiceImpl();

            ticketDTOList = ticketService.getTicketsByShowID((int) newShowTable.getValueAt(newShowTable.getSelectedRow(), 0));

            try {
                ExportFactory.getExportType("XML").export(ticketDTOList);
            } catch (Exception err) {
                err.printStackTrace();
            }

            JOptionPane.showMessageDialog(null, "Tickets for show have been exported successfully!");
        });

        exportToCSV.addActionListener(e -> {
            List<TicketDTO> ticketDTOList;

            TicketService ticketService = new TicketServiceImpl();

            ticketDTOList = ticketService.getTicketsByShowID((int) newShowTable.getValueAt(newShowTable.getSelectedRow(), 0));

            try {
                ExportFactory.getExportType("CSV").export(ticketDTOList);
            } catch (Exception err) {
                err.printStackTrace();
            }

            JOptionPane.showMessageDialog(null, "Tickets for show have been exported successfully!");
        });
        /*
        Cashiers Panel
         */
        cashierPanel = new JPanel();
        cashierTablePanel = new JPanel();

        //UserServiceImpl userService = new UserServiceImpl(new UserRepositoryImpl());
        UserService userService = new UserServiceImpl();

        JButton addCashierButton = new JButton("Add cashier");
        JButton editCashierButton = new JButton("Edit cashier");
        JButton deleteCashierButton = new JButton("Delete cashier");

        cashier = new CashierTable();
        JTable cashierTable = cashier.createTable();
        cashierTable.setPreferredSize(new Dimension(600, 300));
        cashierTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        JScrollPane cashierScrollPane = new JScrollPane(cashierTable);
        cashierScrollPane.setPreferredSize(new Dimension(600, 300));

        cashierTablePanel.add(cashierScrollPane);
        cashierPanel.add(cashierTablePanel);
        cashierPanel.add(addCashierButton);
        cashierPanel.add(editCashierButton);
        cashierPanel.add(deleteCashierButton);

        addCashierButton.addActionListener(e -> {
            JFrame addCashierFrame = new JFrame();
            JPanel addCashierPanel = new JPanel(new GridLayout(2, 0));
            JPanel addCashierInfoPanel = new JPanel();
            JPanel addCashierSubmitButtonPanel = new JPanel();

            addCashierFrame.setName("Add cashier");
            addCashierFrame.setSize(400, 135);
            addCashierFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            addCashierFrame.setResizable(false);

            JLabel cashierUsername = new JLabel("Username: ");
            JTextField cashierUsernameText = new JTextField("", 10);
            JLabel cashierPassword = new JLabel("Password: ");
            JPasswordField cashierPasswordText = new JPasswordField("", 10);
            JButton addCashierSubmitButton = new JButton("Submit");

            addCashierInfoPanel.add(cashierUsername);
            addCashierInfoPanel.add(cashierUsernameText);
            addCashierInfoPanel.add(cashierPassword);
            addCashierInfoPanel.add(cashierPasswordText);
            addCashierSubmitButtonPanel.add(addCashierSubmitButton);
            addCashierPanel.add(addCashierInfoPanel);
            addCashierPanel.add(addCashierSubmitButtonPanel);

            addCashierSubmitButton.addActionListener(e1 -> {
                userService.createUser(new UserDTO(cashierUsernameText.getText(), Encryption.passwordEncryption(cashierPasswordText.getText()), 0));
                refreshCashiersTable();
                addCashierFrame.dispose();
            });

            addCashierFrame.add(addCashierPanel);
            addCashierFrame.setVisible(true);
            addCashierFrame.setLocationRelativeTo(null);
        });

        editCashierButton.addActionListener(e -> {
            JFrame editCashierFrame = new JFrame();
            JPanel editCashierPanel = new JPanel(new GridLayout(2, 0));
            JPanel editCashierInfoPanel = new JPanel();
            JPanel editCashierSubmitButtonPanel = new JPanel();

            editCashierFrame.setName("Add cashier");
            editCashierFrame.setSize(400, 135);
            editCashierFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            editCashierFrame.setResizable(false);

            JLabel cashierUsername = new JLabel("Username: ");
            JTextField cashierUsernameText = new JTextField("", 10);
            JLabel cashierPassword = new JLabel("Password: ");
            JPasswordField cashierPasswordText = new JPasswordField("", 10);
            JButton editCashierSubmitButton = new JButton("Submit");

            editCashierInfoPanel.add(cashierUsername);
            editCashierInfoPanel.add(cashierUsernameText);
            editCashierInfoPanel.add(cashierPassword);
            editCashierInfoPanel.add(cashierPasswordText);
            editCashierSubmitButtonPanel.add(editCashierSubmitButton);
            editCashierPanel.add(editCashierInfoPanel);
            editCashierPanel.add(editCashierSubmitButtonPanel);

            editCashierSubmitButton.addActionListener(e1 -> {
                userService.updateUser((int) newCashierTable.getValueAt(newCashierTable.getSelectedRow(), 0), new UserDTO(cashierUsernameText.getText(), Encryption.passwordEncryption(cashierPasswordText.getText()), 0));
                refreshCashiersTable();
                editCashierFrame.dispose();
            });

            editCashierFrame.add(editCashierPanel);
            editCashierFrame.setVisible(true);
            editCashierFrame.setLocationRelativeTo(null);
        });

        deleteCashierButton.addActionListener(e -> {
            userService.deleteUserByID((int) newCashierTable.getValueAt(newCashierTable.getSelectedRow(), 0));
            refreshCashiersTable();
        });


        /*
        Shows Panel
         */
        showPanel = new JPanel();
        showTablePanel = new JPanel();

        ShowService showService = new ShowServiceImpl();

        JButton addShowButton = new JButton("Add show");
        JButton editShowButton = new JButton("Edit show");
        JButton deleteShowButton = new JButton("Delete show");

        show = new ShowTable();
        JTable showTable = show.createTable();
        showTable.setPreferredSize(new Dimension(600, 300));
        showTable.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        JScrollPane showScrollPane = new JScrollPane(showTable);
        showScrollPane.setPreferredSize(new Dimension(600, 300));

        showTablePanel.add(showScrollPane);
        showPanel.add(showTablePanel);
        showPanel.add(addShowButton);
        showPanel.add(editShowButton);
        showPanel.add(deleteShowButton);

        addShowButton.addActionListener(e -> {
            JFrame addShowFrame = new JFrame();
            JPanel addShowPanel = new JPanel(new GridLayout(2, 0));
            JPanel addShowInfoPanel = new JPanel(new GridLayout(5, 2));
            JPanel addShowSubmitButtonPanel = new JPanel();

            addShowFrame.setSize(375, 300);
            addShowFrame.setName("Add show");
            addShowFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            addShowFrame.setResizable(false);

            JLabel titleLabel = new JLabel("Title: ");
            JLabel genreLabel = new JLabel("Genre: ");
            JLabel distributionLabel = new JLabel("Distribution: ");
            JLabel showDateLabel = new JLabel("Show date: ");
            JLabel availableTicketsLabel = new JLabel("Available tickets: ");

            JTextField titleText = new JTextField(15);
            String[] genre = {"Ballet", "Opera"};
            @SuppressWarnings("unchecked") JComboBox genreComboBox = new JComboBox(genre);
            JTextField distributionText = new JTextField(15);
            JTextField showDateText = new JTextField(15);
            JTextField availableTicketsText = new JTextField(15);

            addShowInfoPanel.add(titleLabel);
            addShowInfoPanel.add(titleText);
            addShowInfoPanel.add(genreLabel);
            addShowInfoPanel.add(genreComboBox);
            addShowInfoPanel.add(distributionLabel);
            addShowInfoPanel.add(distributionText);
            addShowInfoPanel.add(showDateLabel);
            addShowInfoPanel.add(showDateText);
            addShowInfoPanel.add(availableTicketsLabel);
            addShowInfoPanel.add(availableTicketsText);

            JButton addShowSubmitButton = new JButton("Submit");
            addShowSubmitButtonPanel.add(addShowSubmitButton);

            addShowPanel.add(addShowInfoPanel);
            addShowPanel.add(addShowSubmitButtonPanel);

            addShowSubmitButton.addActionListener(e1 -> {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                Date parseDate = null;
                try {
                    parseDate = format.parse(showDateText.getText());
                } catch (ParseException e2) {
                    e2.printStackTrace();
                }
                java.sql.Date sqlDate = null;
                if (parseDate != null) {
                    sqlDate = new java.sql.Date(parseDate.getTime());
                }

                int index = genreComboBox.getSelectedIndex();

                showService.createShow(new ShowDTO(titleText.getText(), genre[index], distributionText.getText(), sqlDate, Integer.parseInt(availableTicketsText.getText()), Integer.parseInt(availableTicketsText.getText())));
                refreshShowsTable();
                addShowFrame.dispose();
            });

            addShowFrame.add(addShowPanel);
            addShowFrame.setVisible(true);
            addShowFrame.setLocationRelativeTo(null);
        });

        editShowButton.addActionListener(e -> {
            JFrame editShowFrame = new JFrame();
            JPanel editShowPanel = new JPanel(new GridLayout(2, 0));
            JPanel editShowInfoPanel = new JPanel(new GridLayout(6, 2));
            JPanel editShowSubmitButtonPanel = new JPanel();

            editShowFrame.setSize(375, 300);
            editShowFrame.setName("Edit show");
            editShowFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            editShowFrame.setResizable(false);

            JLabel titleLabel = new JLabel("Title: ");
            JLabel genreLabel = new JLabel("Genre: ");
            JLabel distributionLabel = new JLabel("Distribution: ");
            JLabel showDateLabel = new JLabel("Show date: ");
            JLabel availableTicketsLabel = new JLabel("Available tickets: ");
            JLabel remainingTicketsLabel = new JLabel("Remaining tickets: ");

            JTextField titleText = new JTextField(15);
            String[] genre = {"Ballet", "Opera"};
            @SuppressWarnings("unchecked") JComboBox genreComboBox = new JComboBox(genre);
            JTextField distributionText = new JTextField(15);
            JTextField showDateText = new JTextField(15);
            JTextField availableTicketsText = new JTextField(15);
            JTextField remainingTicketsText = new JTextField(15);

            editShowInfoPanel.add(titleLabel);
            editShowInfoPanel.add(titleText);
            editShowInfoPanel.add(genreLabel);
            editShowInfoPanel.add(genreComboBox);
            editShowInfoPanel.add(distributionLabel);
            editShowInfoPanel.add(distributionText);
            editShowInfoPanel.add(showDateLabel);
            editShowInfoPanel.add(showDateText);
            editShowInfoPanel.add(availableTicketsLabel);
            editShowInfoPanel.add(availableTicketsText);
            editShowInfoPanel.add(remainingTicketsLabel);
            editShowInfoPanel.add(remainingTicketsText);

            JButton editShowSubmitButton = new JButton("Submit");
            editShowSubmitButtonPanel.add(editShowSubmitButton);

            editShowPanel.add(editShowInfoPanel);
            editShowPanel.add(editShowSubmitButtonPanel);

            editShowSubmitButton.addActionListener(e1 -> {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                Date parseDate = null;
                try {
                    parseDate = format.parse(showDateText.getText());
                } catch (ParseException e2) {
                    e2.printStackTrace();
                }
                java.sql.Date sqlDate = null;
                if (parseDate != null) {
                    sqlDate = new java.sql.Date(parseDate.getTime());
                }

                int index = genreComboBox.getSelectedIndex();

                showService.updateShow((int) newShowTable.getValueAt(newShowTable.getSelectedRow(), 0), new ShowDTO(titleText.getText(), genre[index], distributionText.getText(), sqlDate, Integer.parseInt(availableTicketsText.getText()), Integer.parseInt(availableTicketsText.getText())))
                ;
                refreshShowsTable();
                editShowFrame.dispose();
            });

            editShowFrame.add(editShowPanel);
            editShowFrame.setVisible(true);
            editShowFrame.setLocationRelativeTo(null);
        });

        deleteShowButton.addActionListener(e -> {
            showService.deleteShowByID((int) newShowTable.getValueAt(newShowTable.getSelectedRow(), 0));
            refreshShowsTable();
        });
        /*
        Final settings
         */
        add(finalPanel);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @SuppressWarnings("Duplicates")
    private void refreshCashiersTable() {
        newCashierTable = cashier.createTable();
        newCashierTable.setPreferredSize(new Dimension(600, 500));
        newCashierTable.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        JScrollPane newPane = new JScrollPane(newCashierTable);
        newPane.setPreferredSize(new Dimension(600, 300));

        cashierTablePanel.removeAll();
        cashierTablePanel.add(newPane);
        cashierTablePanel.revalidate();
        cashierTablePanel.repaint();
    }

    @SuppressWarnings("Duplicates")
    private void refreshShowsTable() {
        newShowTable = show.createTable();
        newShowTable.setPreferredSize(new Dimension(600, 500));
        newShowTable.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        JScrollPane newPane = new JScrollPane(newShowTable);
        newPane.setPreferredSize(new Dimension(600, 300));

        showTablePanel.removeAll();
        showTablePanel.add(newPane);
        showTablePanel.revalidate();
        showTablePanel.repaint();
    }
}
