import org.junit.Test;
import presentation.util.Encryption;

import static org.junit.Assert.*;

public class ApplicationTest {

    @Test
    public void encryptionTest() {
        String password = "parola";
        String encryptedPassword = Encryption.passwordEncryption(password);

        assertEquals(encryptedPassword, "8287458823facb8ff918dbfabcd22ccb");
    }
}
